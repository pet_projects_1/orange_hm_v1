import time
import pytest
import allure
from tests_data.Test_Data import *
"""pytest -v -m add_employee tests/ui_test.py --alluredir=allure_results """


@allure.title("add_employee")
@pytest.mark.regression
@pytest.mark.add_employee
def test_add_employee(browser):
    d = PIMPageData()
    side_panel = browser.login_by_credentials(admin_username, admin_password)
    pim_panel = side_panel.select_section('PIM')
    pim_panel.add_employee(d.employee)
    side_panel.check_url_endpoint("empNumber")
    side_panel.logout()


@allure.title("add_user")
@pytest.mark.regression
@pytest.mark.add_user
def test_add_user(browser):
    d = AdminPageAddUser()
    side_panel = browser.login_by_credentials(admin_username, admin_password)
    admin_panel = side_panel.select_section('Admin')
    admin_panel.add_user(d.dp_data, d.input_data, d.employee)
    admin_panel.search_user(d.username_dict, d.dp_data, d.employee)
    admin_panel.check_record_found(d.user_row)
    admin_panel.logout()


@allure.title("login_user")
@pytest.mark.regression
@pytest.mark.login_user
def test_login_user(browser):
    d = AdminPageAddUser()
    side_panel = browser.login_by_credentials(d.input_data.get("Username"), d.input_data.get("Password"))
    side_panel.logout()


@allure.title("add_work_shift")
@pytest.mark.regression
@pytest.mark.add_work_shift
def test_add_work_shift(browser):
    d = AdminPageAddUser()
    side_panel = browser.login_by_credentials(admin_username, admin_password)
    admin_panel = side_panel.select_section('Admin')
    admin_panel.select_tab("Job")
    admin_panel.select_tab_dropdown_item("Work Shifts")
    admin_panel.add_work_shift(d.shift_name_dict, d.shift_time_dict)
    admin_panel.search_work_shifts(d.work_shift_row_text)
    admin_panel.logout()

@allure.title("delete_work_shift")
@pytest.mark.regression
@pytest.mark.delete_work_shift
def test_delete_work_shift(browser):
    d = AdminPageAddUser()
    side_panel = browser.login_by_credentials(admin_username, admin_password)
    admin_panel = side_panel.select_section('Admin')
    admin_panel.select_tab("Job")
    admin_panel.select_tab_dropdown_item("Work Shifts")
    admin_panel.delete_work_shift(d.work_shift_row_text)
    admin_panel.logout()

@allure.title("add_candidate")
@pytest.mark.regression
@pytest.mark.add_candidate
def test_add_candidate(browser):
    d = RecruitmentDate()
    side_panel = browser.login_by_credentials(admin_username, admin_password)
    recruitment_panel = side_panel.select_section('Recruitment')
    recruitment_panel.add_candidate(d.resume_file, d.date, d.full_name, d.vacancy_dict, d.input_dict, d.textarea)
    recruitment_panel.logout()


@allure.title("delete_employee")
@pytest.mark.regression
@pytest.mark.delete_employee
def test_delete_employee(browser):
    d = PIMPageData()
    side_panel = browser.login_by_credentials(admin_username, admin_password)
    pim_panel = side_panel.select_section('PIM')
    pim_panel.delete_employee(d.employee_check)
    pim_panel.logout()








