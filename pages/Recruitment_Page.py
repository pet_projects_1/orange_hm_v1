import time
import allure
import os
from base_page.Base_Page import Base
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver


class RecruitmentPage(Base):
    def __init__(self, driver: WebDriver):
        self.driver = driver

    def resume_download(self, file_path: str):
        exp_file_name = os.path.basename(file_path)
        with allure.step(f'Download resume by path: {file_path}'):
            finput = self.find_element_present((By.CLASS_NAME, 'oxd-file-input'))
            finput.send_keys(file_path)
        with allure.step(f'Verification of the downloaded resume file'):
            file_name = self.find_element_present((By.CLASS_NAME, 'oxd-file-div--active'))
            file_name_text = file_name.text

            assert exp_file_name in file_name_text, f"Resume is not downloaded. CURR:{file_name_text} instead of EXP:{exp_file_name}"

    @allure.step('add_candidate')
    def add_candidate(self, resume_path: str, date: str, full_name: dict, vacancy_dict: dict, input_dict: dict, text_area: dict):
        with allure.step(f'Resume upload'):
            self.action_chains_click(self.find_element_clicable((By.CSS_SELECTOR, ".orangehrm-header-container button")))
            absolute_path = os.path.abspath(resume_path)
            self.resume_download(absolute_path)
        with allure.step(f'Set date of application: {date}'):
            self.action_chains_click(self.find_element_clicable((By.CLASS_NAME, 'oxd-date-input')))
            self.action_chains_click(self.find_element_present((By.CSS_SELECTOR, ".oxd-calendar-date.--today")))
        for k, v in full_name.items():
            with allure.step(f'Fill out field {k} as {v}'):
                field = self.find_element_visible((By.NAME, k))
                self.send_keys(field, v)
        self.select_dropdown_value_by_lable(vacancy_dict)
        self.set_inputs_by_lable(input_dict)
        note_obj = self.get_obj_by_label(list(text_area.keys())[0])
        textarea_el = self.get_obj_element(note_obj, (By.TAG_NAME, "textarea"))
        self.send_keys(textarea_el, list(text_area.values())[0])
        self.action_chains_click(self.find_element_present((By.CSS_SELECTOR, '[type="checkbox"]')))
        self.action_chains_click(self.find_element_clicable((By.CSS_SELECTOR, ".orangehrm-left-space")))
        status = self.find_element_present((By.CLASS_NAME, 'orangehrm-recruitment-status')).text
        assert status == 'Status: Application Initiated'




