from base_page.Base_Page import Base
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from pages.Side_panel import SidePanel
import allure


app_url = "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login"


class LoginPage(Base):
    def __init__(self, driver: WebDriver):
        self.driver = driver
        self.driver.get(app_url)
        self.driver.maximize_window()

    def login_by_credentials(self, username, password):
        with allure.step(f'Login with creds: {username}, {password}'):
            user_fild = self.find_element_present((By.NAME, "username"))
            self.send_keys(user_fild, username)
            password_fild = self.find_element_present((By.NAME, "password"))
            self.send_keys(password_fild, password)
            login_btn = self.find_element_clicable((By.TAG_NAME, "button"))
            allure.attach(self.driver.get_screenshot_as_png(), attachment_type=allure.attachment_type.PNG)
            self.action_chains_click(login_btn)
            self.check_url_endpoint("dashboard")
            return SidePanel(self.driver)
