from base_page.Base_Page import Base
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from pages.Admin_Page import AdminPage
from pages.PIM_Page import PIMPage
from pages.Recruitment_Page import RecruitmentPage
import allure


class SidePanel(Base):
    def __init__(self, driver: WebDriver):
        self.driver = driver

    def select_section(self, section_name: str):
        with allure.step(f'select section: {section_name}'):
            elements = self.find_elements_present((By.CSS_SELECTOR, '.oxd-sidepanel-body a'))
            section = [x for x in elements if section_name in x.text]
            assert section, "No section found"
            self.action_chains_click(section[0])
            self.check_url_endpoint(section_name.lower())
            if section_name == 'Admin':
                return AdminPage(self.driver)
            if section_name == 'PIM':
                return PIMPage(self.driver)
            if section_name == 'Recruitment':
                return RecruitmentPage(self.driver)


