import time

from base_page.Base_Page import Base
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
import allure

class AdminPage(Base):
    def __init__(self, driver: WebDriver):
        self.driver = driver

    def click_add_btn(self):
        btn = self.find_element_clicable((By.CLASS_NAME, 'bi-plus'))
        self.action_chains_click(btn)
        time.sleep(2)

    def add_user(self, dp_dict: dict, input_dict: dict, employee: str):
        self.click_add_btn()
        self.select_employee(employee)
        self.select_dropdown_value_by_lable(dp_dict)
        self.set_inputs_by_lable(input_dict)
        time.sleep(5)
        self.click_save_btn()
        self.check_url_endpoint('viewSystemUsers', 20)

    def select_employee(self, employee_name: str):
        split_name = employee_name.split()
        self.set_inputs_by_lable({"Employee Name": split_name[0]})
        time.sleep(5)
        option = self.find_element_clicable((By.CLASS_NAME, 'oxd-autocomplete-option'))
        time.sleep(5)
        self.action_chains_click(option)

    def click_save_btn(self):
        save_btn = self.find_element_clicable((By.CSS_SELECTOR, '.oxd-form-actions .orangehrm-left-space'))
        self.action_chains_click(save_btn)

    def click_cancel_btn(self):
        cancel_btn = self.find_element_clicable((By.CSS_SELECTOR, '.oxd-form-actions .oxd-button--ghost'))
        self.action_chains_click(cancel_btn)

    def click_user_search(self):
        user_search_btn = self.find_element_clicable((By.CSS_SELECTOR, '.orangehrm-left-space'))
        self.action_chains_click(user_search_btn)

    def click_user_reset_btn(self):
        user_reset_btn = self.find_element_clicable((By.CSS_SELECTOR, '.oxd-button--ghost'))
        self.action_chains_click(user_reset_btn)

    def search_user(self, input_dict: dict, dp_dict: dict, employee: str):
        self.set_inputs_by_lable(input_dict)
        self.select_dropdown_value_by_lable(dp_dict)
        self.select_employee(employee)
        self.click_user_search()
        time.sleep(15)
        record = self.find_element_present((By.CSS_SELECTOR, '.orangehrm-vertical-padding'))
        record_text = record.text
        assert record_text == "(1) Record Found", f"CURR: {record_text} instead of EXP: (1) Record Found"

    def check_record_found(self, exp: list):
        record = self.find_element_present((By.CLASS_NAME, 'oxd-table-body')).text
        for text in exp:
            assert text in record, f"{text} is not found in  '{record}'"

    def click_add_btn_job(self):
        add_btn_job = self.find_element_clicable((By.CSS_SELECTOR, '.oxd-button--secondary'))
        self.action_chains_click(add_btn_job)

    def select_tab(self, tab_text: str):
        self.el_from_els_by_text((By.CLASS_NAME, 'oxd-topbar-body-nav-tab'), tab_text, True)

    def select_tab_dropdown_item(self, item_text: str):
        self.el_from_els_by_text((By.CLASS_NAME, 'oxd-topbar-body-nav-tab-link'), item_text, True)

    def add_work_shift(self, shift_name: dict, dict_hours: dict):
        self.action_chains_click(self.find_element_clicable((By.CLASS_NAME, 'oxd-button--secondary')))
        self.set_inputs_by_lable(shift_name)
        for k, v in dict_hours.items():
            el = self.get_obj_by_label(k)
            el_input = self.get_obj_element(el, (By.TAG_NAME, 'input'))
            self.action_chains_click(el_input)
            for time_key, time_value in v[3].items():
                input_btn_selector = f"oxd-time-{time_key}-input-{time_value[1]}"
                arrow_btn = self.find_element_clicable((By.CLASS_NAME, input_btn_selector))
                for i in range(time_value[0]):
                    self.action_chains_click(arrow_btn)
        duration_per_day = self.find_element_visible((By.CLASS_NAME, 'orangehrm-workshift-duration'))
        duration_per_day_text = duration_per_day.text
        assert duration_per_day_text == "3.00", f"CURR: {duration_per_day_text} instead of EXP: 3.00"
        self.action_chains_click(self.find_element_clicable((By.CSS_SELECTOR, '.orangehrm-left-space')))

    def search_work_shifts(self, exp_work_shift_row_text: str):
        rows = self.find_elements_present((By.CLASS_NAME, 'oxd-table-card'))
        obj = None
        curr_rows_texts = []
        for row in rows:
            text = row.text
            curr_rows_texts.append(text)
            if exp_work_shift_row_text in text:
                obj = row
                break
        assert obj, f"EXP:'{exp_work_shift_row_text}' row text is not found in: \n {curr_rows_texts}"
        return obj

    @allure.step('delete_employee')
    def delete_work_shift(self, work_shift_data: str):
        def get_row(data, check_deletion=False):
            employee_rows = self.find_elements_present((By.CLASS_NAME, 'oxd-table-card'))
            obj = None
            work_shift_rows_texts = []
            for employee_row in employee_rows:
                text = employee_row.text
                work_shift_rows_texts.append(text)
                if data in text:
                    obj = employee_row
                    break
            if check_deletion:
                assert not obj, f"EXP:'{work_shift_data}' row text found in: \n {work_shift_rows_texts}"
            else:
                assert obj, f"EXP:'{work_shift_data}' row text is not found in: \n {work_shift_rows_texts}"
                return obj
        with allure.step(f"Click the delete btn for employee: {work_shift_data}"):
            wanted_obj = get_row(work_shift_data)
            self.action_chains_click(self.get_obj_element(wanted_obj, (By.CSS_SELECTOR, '.bi-trash')))
            self.confirm()
        with allure.step(f"Check employee {work_shift_data} deletion"):
            get_row(work_shift_data, check_deletion=True)
