import time
import allure
from base_page.Base_Page import Base
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver


class PIMPage(Base):
    def __init__(self, driver: WebDriver):
        self.driver = driver

    def add_employee(self, dict_names: dict):
        with allure.step('Click Add button'):
            btn = self.find_element_clicable((By.CSS_SELECTOR, '.orangehrm-header-container button'))
            self.action_chains_click(btn)
            time.sleep(2)
        for k, v in dict_names.items():
            with allure.step(f'Fill out field {k} as {v}'):
                field = self.find_element_visible((By.NAME, k))
                self.send_keys(field, v)
        allure.attach(self.driver.get_screenshot_as_png(), attachment_type=allure.attachment_type.PNG)
        with allure.step('Click Save button'):
            save_btn = self.find_element_clicable((By.CSS_SELECTOR, '[type="submit"]'))
            self.action_chains_click(save_btn)
            time.sleep(5)

    @allure.step('delete_employee')
    def delete_employee(self, employee_data: str):
        def get_row(data, check_deletion=False):
            employee_rows = self.find_elements_present((By.CSS_SELECTOR, '.oxd-table-row--clickable'))
            obj = None
            employee_rows_texts = []
            for employee_row in employee_rows:
                text = employee_row.text
                employee_rows_texts.append(text)
                if data in text:
                    obj = employee_row
                    break
            if check_deletion:
                assert not obj, f"EXP:'{employee_data}' row text found in: \n {employee_rows_texts}"
            else:
                assert obj, f"EXP:'{employee_data}' row text is not found in: \n {employee_rows_texts}"
                return obj
        with allure.step(f"Click the delete btn for employee: {employee_data}"):
            wanted_obj = get_row(employee_data)
            self.action_chains_click(self.get_obj_element(wanted_obj, (By.CSS_SELECTOR, '.bi-trash')))
            self.confirm()
        with allure.step(f"Check employee {employee_data} deletion"):
            get_row(employee_data, check_deletion=True)


