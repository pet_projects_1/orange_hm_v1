from selenium.webdriver.common.by import By
import allure
import os
import pywinauto
from selenium.webdriver.common import keys as Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.remote.webdriver import WebDriver
from datetime import datetime


class Base:
    def __int__(self, driver: WebDriver):
        self.driver = driver

    def action_chains_click(self, el):
        element = ActionChains(self.driver)
        element.move_to_element(el)
        element.click(el)
        element.perform()

    def move_to_element(self, el):
        element = ActionChains(self.driver)
        element.move_to_element(el)
        return element.perform()

    def send_keys(self, el, text):
        el.clear()
        el.send_keys(text)

    def check_url_endpoint(self, endpoint, time=60):
        WebDriverWait(self.driver, time).until(EC.url_contains(endpoint))

    def find_element_clicable(self, locator, time=30):
        return WebDriverWait(self.driver, time).until(
                EC.element_to_be_clickable(locator), message=f"Can't find element by locator {locator}")

    def find_element_present(self, locator, time=30):
        return WebDriverWait(self.driver, time).until(
                EC.presence_of_element_located(locator), message=f"Can't find element by locator {locator}")

    def find_elements_present(self, locator, time=30):
        return WebDriverWait(self.driver, time).until(
                EC.presence_of_all_elements_located(locator), message=f"Can't find element by locator {locator}")

    def find_element_visible(self, locator, time=30):
        return WebDriverWait(self.driver, time).until(
                EC.visibility_of_element_located(locator), message=f"Can't find element by locator {locator}")

    @staticmethod
    def get_obj_elements(obj, locator, time=20):
        return WebDriverWait(obj, time).until(EC.presence_of_all_elements_located(locator),
                                              message=f"Can't find element by locator {locator}")

    @staticmethod
    def get_obj_element(obj, locator, time=20):
        return WebDriverWait(obj, time).until(EC.presence_of_element_located(locator),
                                              message=f"Can't find element by locator {locator}")

    def get_obj_by_label(self, lable_text: str):
        elements = self.find_elements_present((By.CLASS_NAME, "oxd-grid-item"))
        obj = None
        for element in elements:
            label = self.get_obj_element(element, (By.TAG_NAME, "label"))
            if label.text == lable_text:
                obj = element
                break
        assert obj, f"'{lable_text}' label is not found"
        return obj

    def set_inputs_by_lable(self, data_dict: dict):
        for k, v in data_dict.items():
            el = self.get_obj_by_label(k)
            el_input = self.get_obj_element(el, (By.TAG_NAME, "input"))
            self.send_keys(el_input, v)

    def select_dropdown_value_by_lable(self, data_dict):
        for k, v in data_dict.items():
            with allure.step(f"select dropdown {k} value: {v}"):
                with allure.step("Open dropdown"):
                    dr = self.get_obj_by_label(k)
                    arrow = self.get_obj_element(dr, (By.CLASS_NAME, "oxd-select-text--arrow"))
                    self.action_chains_click(arrow)
                with allure.step("Select the value"):
                    dr_items = self.find_elements_present((By.CLASS_NAME, 'oxd-select-option'))
                    dr_item = [x for x in dr_items if x.text == v]
                    assert dr_item, "dr_item is empty"
                    self.action_chains_click(dr_item[0])
                with allure.step("Check the value"):
                    el_input = self.get_obj_element(dr, (By.CLASS_NAME, "oxd-select-text-input"))
                    input_text = el_input.text
                    assert input_text == v, f"CURR: {input_text} instead of EXP: {v}"

    def el_from_els_by_text(self, locator, text: str, click=None):
        elements = self.find_elements_present(locator)
        obj = None
        for element in elements:
            if element.text == text:
                obj = element
                break
        assert obj, f"'{text}' is not found"
        if click:
            self.action_chains_click(obj)

    def context_menu(self, item_text: str):
        ''' item_text can be as "About", "Support", "Change Password", "Logout" '''
        self.action_chains_click(self.find_element_clicable((By.CLASS_NAME, "oxd-userdropdown-icon")))
        menu = self.find_elements_present((By.CSS_SELECTOR, '.oxd-dropdown-menu li'))
        obj = None
        for element in menu:
            if element.text == item_text:
                obj = element
                break
        assert obj, f"'{item_text}' is not found"
        self.action_chains_click(obj)

    def logout(self):
        self.context_menu("Logout")
        self.check_url_endpoint("login")

    def confirm(self):
        self.action_chains_click(self.find_element_clicable((By.CSS_SELECTOR, ".orangehrm-dialog-popup .oxd-button--label-danger")))

    # def upload_file_by_pywinauto(self, filename, username, processname):
    #     timeout = 20
    #     with allure.step("Upload file by pywinauto: {}".format(filename)):
    #         newapp = pywinauto.application.Application()
    #         process = self.get_pid(username, processname)
    #         newapp.connect(process=int(process))
    #         w_handle = pywinauto.application.timings.wait_until_passes(
    #             timeout, 0.5, lambda: pywinauto.findwindows.find_windows(class_name='#32770', title=u'Open')[0])
    #         windowOpen = newapp.window(handle=w_handle)
    #         windowOpen.set_focus()
    #         windowOpen["Edit"].type_keys('{}'.format(filename))  # put file path
    #         windowOpen.wait('ready')
    #         windowOpen['OpenButton'].double_click()
    #         windowOpen.wait_not('visible', timeout)
    #
    # def get_pid(self, username, process_name):
    #     def cut_str_edge(value):
    #         return value.rstrip(',').lstrip(',')
    #
    #     def split_str(value):
    #         return value.split(':')
    #
    #     query = 'TASKLIST /FI "USERNAME eq {}" /FI "IMAGENAME eq {}" /FO LIST'.format(username, process_name)
    #     process_obj = os.popen(query)
    #     process_str = str(process_obj.read())
    #     separated = process_str.replace('\n', ',')
    #     cut_off = cut_str_edge(separated)
    #     cut_off_spaces = cut_off.replace('  ', '')
    #     splited = cut_off_spaces.split(',')
    #     removed_last_item = splited[:-1]
    #     process_dict = {}
    #     for i in removed_last_item:
    #         v = split_str(i)
    #         process_dict.update(dict({v[0]: v[1]}))
    #     assert len(process_dict) > 0
    #     return process_dict.get('PID')
