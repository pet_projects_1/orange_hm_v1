from datetime import datetime
admin_username, admin_password = "Admin", "admin123"
import os


class AdminPageAddUser:
    def __init__(self):
        self.employee = "Oleg Delf Collins"
        self.dp_data = {"User Role": "ESS", "Status": "Enabled"}
        self.username_dict = {"Username": "auto_oleg_username"}
        self.input_data = {"Username": "auto_oleg_username", "Password": "Auto_1E@", "Confirm Password": "Auto_1E@"}
        self.user_row = [' '.join(self.employee.split()[0::2])] + list(self.dp_data.values()) + list(self.username_dict.values())
        self.tab_text = {"Job", "Work Shifts"}
        self.shift_name_dict = {"Shift Name": "John Simmons"}
        self.tab_text_hours = ["From", "To"]
        self.shift_time_dict = {"From": ["11", "15", "AM", {"hour": [2, "up"], "minute": [1, "up"]}],
                                "To": ["02", "15", "PM", {"hour": [3, "down"], "minute": [1, "up"]}]}
        self.work_shift_row_text = f'{self.shift_name_dict.get("Shift Name")}'\
                                   f'\n{":".join(self.shift_time_dict.get("From")[:2])} {self.shift_time_dict.get("From")[2]}'\
                                   f'\n{":".join(self.shift_time_dict.get("To")[:2])} {self.shift_time_dict.get("To")[2]}\n3.00'


class PIMPageData:
    def __init__(self):
        self.employee = {"firstName": "Oleg", "middleName": "Delf", "lastName": "Collins"}
        self.employee_check = f'{self.employee.get("firstName")} {self.employee.get("middleName")}\n'\
                              f'{self.employee.get("lastName")}'


class RecruitmentDate:
    def __init__(self):
        self.resume_file = 'tests_data\\jaguar.pdf'
        self.date = '2023-04-17'
        self.winprocess = 'explorer.exe'
        self.winusername = 'Lenoute'
        self.full_name = {"firstName": "Liza", "middleName": "Dudak", "lastName": "Olegivna"}
        self.vacancy_dict = {"Vacancy": "Junior Account Assistant"}
        self.input_dict = {"Email": "lizdud22@gmail.com", "Contact Number": "+380967754343", "Keywords": "Python, Selenium, Jankins"}
        self.textarea = {"Notes": "Allure"}
