# orange_hm_v1

## Prerequisites for Windows
- Download and install git https://git-scm.com/downloads
- Download and install python https://www.python.org/downloads/
- Make sure that pythons scripts paths was added to the "System Variables" => "Path" on your Windows machine
- Install pip
```
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
py get-pip.py
```
- Download chrome webdriver compatible with your chrome browser https://chromedriver.chromium.org/downloads
- Add the path where chromedriver is located to the "System Variables" => "Path" on your Windows machine
- Restart your Windows

## Project setup (command line)

Clone the repository: 
```
git clone https://gitlab.com/pet_projects_1/orange_hm_v1.git orange
```

Then go to the project root:
```
cd orange
```

Install virtualenv
```
py -m pip install virtualenv
```

Create virtual environment in project root:
```
py -m venv env
```

Activate virtual environment:
```
env\Scripts\activate
```

Install project dependencies:
```
pip install -r requirements.txt
```

## Run tests

```
pytest tests
```

