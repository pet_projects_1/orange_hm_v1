from selenium.webdriver.common.selenium_manager import SeleniumManager
from selenium import webdriver
import pytest
from pages.Login_Page import LoginPage
import allure


@pytest.fixture(scope="function")
def browser():
    driver = webdriver.Chrome()
    yield LoginPage(driver)
    try:
        allure.attach(driver.get_screenshot_as_png(), attachment_type=allure.attachment_type.PNG)
    except Exception as e:
        print(e)
    driver.quit()
